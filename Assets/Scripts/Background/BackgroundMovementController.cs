﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMovementController : MonoBehaviour {

    public Transform mainCharacter;
    private Renderer br;
    private float backgroundStartMovementPosition;
    private float velociyBG = 0.01f;

    void Start()
    {
        //0.4f : For posible errors when background descends
        backgroundStartMovementPosition = transform.position.y - 0.4f;
        br = GetComponent<Renderer>();
        br.material.mainTextureOffset = new Vector2(0f, velociyBG * (backgroundStartMovementPosition));
    }
    
    void Update()
    {
        if (mainCharacter.transform.position.y > (backgroundStartMovementPosition) && mainCharacter.GetComponent<MCStatus>().lives > 0) {
            br.material.mainTextureOffset = new Vector2(0f, velociyBG * (mainCharacter.transform.position.y));
            transform.position = new Vector3(transform.position.x, mainCharacter.transform.position.y, transform.position.z);
        }
    }

}
