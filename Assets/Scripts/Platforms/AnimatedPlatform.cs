﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedPlatform : MonoBehaviour {

    private float leftLimit;
    private float rightLimit;
    private float limitMargins = 1.5f;

    private float velocity = 5f;
    private bool goRight;

    void Start () {
        rightLimit = GameObject.FindGameObjectWithTag("RightLimitElement").transform.position.x - limitMargins;
        leftLimit = GameObject.FindGameObjectWithTag("LeftLimitElement").transform.position.x + limitMargins;
    }
	
	void Update () {
        if (transform.position.x <= leftLimit) goRight = true;
        if (transform.position.x >= rightLimit) goRight = false;

        if(goRight) {
            transform.position = new Vector3(transform.position.x + (velocity * Time.deltaTime), transform.position.y, transform.position.z);
        } else {
            transform.position = new Vector3(transform.position.x - (velocity * Time.deltaTime), transform.position.y, transform.position.z);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Player") {
            collision.gameObject.transform.SetParent(transform);
        }
    }

    public void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.tag == "Player") {
            collision.gameObject.transform.SetParent(null);
        }
    }
}
