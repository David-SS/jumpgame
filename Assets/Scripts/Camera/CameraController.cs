﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject mainCharacter;
    private float cameraOffset;
    private float positionIncrement = 3f;
    
    void Start() {
        cameraOffset = mainCharacter.transform.position.y - transform.position.y + positionIncrement;
    }
    
    void Update() {
        if (mainCharacter.GetComponent<MCStatus>().isAlive())
            transform.position = new Vector3(transform.position.x, mainCharacter.transform.position.y + cameraOffset, transform.position.z);
    }
}
