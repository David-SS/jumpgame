﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftLimitPass : MonoBehaviour {

    private Transform rightLimit;
    private Transform mainCharacter;
    
    void Start () {
        rightLimit = GameObject.FindGameObjectWithTag("RightLimitElement").transform;
        mainCharacter = GameObject.FindGameObjectWithTag("Player").transform;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player") {
            mainCharacter.position = new Vector3(rightLimit.position.x - 1f, mainCharacter.position.y, mainCharacter.position.z);
        }
    }
}
