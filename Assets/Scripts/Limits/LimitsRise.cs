﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitsRise : MonoBehaviour {

    public Transform mainCharacter;
    private float offsetLimits;
    private float beforeLimitPosition;
    
    void Start()
    {
        beforeLimitPosition = -100f;
        offsetLimits = transform.position.y - mainCharacter.position.y;
    }
    
    void Update()
    {
        if (beforeLimitPosition < mainCharacter.position.y)
        {
            beforeLimitPosition = mainCharacter.position.y;
            transform.position = new Vector3(transform.position.x, mainCharacter.position.y + offsetLimits, transform.position.z);
        }
    }
}
