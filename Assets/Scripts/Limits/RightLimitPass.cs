﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightLimitPass : MonoBehaviour {

    private Transform leftLimit;
    private Transform mainCharacter;
    
    void Start () {
        leftLimit = GameObject.FindGameObjectWithTag("LeftLimitElement").transform;
        mainCharacter = GameObject.FindGameObjectWithTag("Player").transform;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player") {
            mainCharacter.position = new Vector3(leftLimit.position.x + 1f, mainCharacter.position.y, mainCharacter.position.z);
        }
    }
}
