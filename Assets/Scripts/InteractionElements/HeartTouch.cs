﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartTouch : MonoBehaviour {
    public void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Player") {
            collision.gameObject.GetComponent<MCStatus>().increaseLives();
            Destroy(this.gameObject);
        }
    }
}
