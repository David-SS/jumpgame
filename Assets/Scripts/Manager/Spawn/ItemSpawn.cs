﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawn : MonoBehaviour {

    private int platformToAddItem = 10;
    private int minPlaftformsToAddItem = 20;
    private int maxPlaftformsToAddItem = 40;

    private List<GameObject> itemList = new List<GameObject>();
    public GameObject powerItem;
    public GameObject heartItem;

    // Use this for initialization
    void Start () {
        itemList.Add(powerItem);
        itemList.Add(heartItem);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void addItemToPlatform(GameObject platform) {
        platformToAddItem--;
        if (platformToAddItem == 0) {
            platformToAddItem = Random.Range(minPlaftformsToAddItem, maxPlaftformsToAddItem);
            GameObject item = Instantiate(itemList[Random.Range(0,itemList.Count)], platform.transform.Find("ItemPosition").transform.position, Quaternion.identity);
            item.transform.SetParent(platform.transform);
        }
    }
}
