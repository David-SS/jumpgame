﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawn : MonoBehaviour {

    private float leftLimit;
    private float rightLimit;
    private float limitMargins = 1.5f;

    private Transform mainCharacter;
    private float mainCharacterLastTopPosition;
    private float nextPlatformPosition;
    private float platformYAxisIncrement = 5f;

    private List<GameObject> platformList = new List<GameObject>();
    public GameObject normalPlatform;
    public GameObject animatedPlatform;
    public GameObject pointyPlatform;
    private Queue<GameObject> generatedPlatforms = new Queue<GameObject>();
    private int maxAlivePlatforms = 15;

    void Start() 
    {
        rightLimit = GameObject.FindGameObjectWithTag("RightLimitElement").transform.position.x - limitMargins;
        leftLimit = GameObject.FindGameObjectWithTag("LeftLimitElement").transform.position.x + limitMargins;
        mainCharacter = GameObject.FindGameObjectWithTag("Player").transform;
        mainCharacterLastTopPosition = mainCharacter.position.y;

        platformList.Add(normalPlatform);
        platformList.Add(animatedPlatform);
        platformList.Add(pointyPlatform);
        nextPlatformPosition = mainCharacter.position.y - 1f;
        createPlatform(); //First Platform
    }

    void Update()
    {
        if (mainCharacter.position.y >= (mainCharacterLastTopPosition + platformYAxisIncrement)) {
            mainCharacterLastTopPosition += platformYAxisIncrement;
            createPlatform();
        }
    }

    void createPlatform() {
        GameObject platform = instantiatePlataforms();
        generatedPlatforms.Enqueue(platform);
        if (generatedPlatforms.Count == maxAlivePlatforms) Destroy(generatedPlatforms.Dequeue().gameObject);
        GetComponent<ItemSpawn>().addItemToPlatform(platform);
    }

    GameObject instantiatePlataforms() {
        nextPlatformPosition += platformYAxisIncrement;
        return Instantiate(platformList[Random.Range(0, platformList.Count)], new Vector3(Random.Range(leftLimit, rightLimit), nextPlatformPosition, 3.5f), Quaternion.identity);
    }

}
