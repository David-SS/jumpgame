﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuManager : MonoBehaviour {
    public static bool GameIsPaused = false;

    public GameObject PauseMenuUI;
    public GameObject ScoreUI;
    public GameObject LivesUI;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        Debug.Log("RESUME");
        PauseMenuUI.SetActive(false);
        ScoreUI.SetActive(true);
        LivesUI.SetActive(true);
        Time.timeScale = 1.0f;
        GameIsPaused = false;
    }

    void Pause()
    {
        Debug.Log("PAUSE");
        PauseMenuUI.SetActive(true);
        ScoreUI.SetActive(false);
        LivesUI.SetActive(false);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void Restart()
    {
        Debug.Log("RESTART GAME");
        SceneManager.LoadScene("MainScene");
        Resume();
    }

    public void Menu()
    {
        Debug.Log("GO MAIN MENU");
        SceneManager.LoadScene("MainMenu");
        Resume();
    }

    public void Exit()
    {
        Debug.Log("EXIT");
        Application.Quit();
    }
}
