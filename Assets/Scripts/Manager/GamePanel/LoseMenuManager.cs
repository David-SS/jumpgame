﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LoseMenuManager : MonoBehaviour {

    public void TryAgain()
    {
        Debug.Log("GO MAIN SCENE");
        SceneManager.LoadScene("MainScene");
    }

    public void GoMenu()
    {
        Debug.Log("GO MAIN MENU");
        SceneManager.LoadScene("MainMenu");
    }
}
