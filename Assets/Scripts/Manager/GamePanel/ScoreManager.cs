﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    private static int score;
    private static int extraScore;
    private static int nextExtraScore;
    private int maxExtraScore = 3;
    private bool decreaseExtraScore = false;

    public Text scoreText;
    public Text extraScoreText;
    
    void Start() {
        score = 0;
        extraScore = 0;
        nextExtraScore = maxExtraScore;
    }
    
    void Update() {
        if (!decreaseExtraScore && nextExtraScore != 0) {
            decreaseExtraScore = true;
            StartCoroutine(decreaseNextExtraScore());
        }
        updateTexts();
    }

    private void updateTexts() {
        scoreText.text = ""+score;
        extraScoreText.text = extraScore + "\n(" + nextExtraScore + ")";
    }

    public void increaseScore() {
        StopAllCoroutines();
        score += 1;
        extraScore += nextExtraScore;
        nextExtraScore = maxExtraScore;
        decreaseExtraScore = false;
    }

    IEnumerator decreaseNextExtraScore() {
        for (int i = 0; i<maxExtraScore; i++) {
            yield return new WaitForSeconds(1);
            nextExtraScore -= 1;
        }
        decreaseExtraScore = false;
    }


}
