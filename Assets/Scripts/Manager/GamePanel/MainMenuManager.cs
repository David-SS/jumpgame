﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {

    public void Play()
    {
        Debug.Log("GO MAIN SCENE");
        SceneManager.LoadScene("MainScene");
    }

    public void Exit()
    {
        Debug.Log("EXIT");
        Application.Quit();
    }

}
