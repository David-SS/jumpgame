﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivesManager : MonoBehaviour {
    
    public Text lives;

    public void setLives(int newLives) {
        lives.text = newLives.ToString();
    }
}
