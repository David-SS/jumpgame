﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MCMovement : MonoBehaviour {

    private float velocity = 8f;
    private bool isGrounded = true;
    private float lastMCPosition;

    private Animator animator;
    private LayerMask groundLayer;

    private MCSounds mcsounds;
    private int levelReachedSound = 0;
    public GameObject scorePanel;

    void Start ()
    {
        animator = GetComponent<Animator>();
        mcsounds = GetComponent<MCSounds>();
        groundLayer = LayerMask.GetMask("Ground");
        lastMCPosition = this.transform.position.y;
    }
    
    void Update()
    {        
        isGrounded = Physics2D.Linecast(transform.position, GameObject.Find("GroundCheck").transform.position, groundLayer);
        characterMovement();
        characterJump();
        isOnNextPlatform();
    }

    private void characterMovement()
    {
        float movement = Input.GetAxisRaw("Horizontal");

        if (movement == 0) {
            animator.SetBool("Walk", false);
        }
        else {
            animator.SetBool("Walk", true);
            transform.position = new Vector3(transform.position.x + movement * velocity * Time.deltaTime, transform.position.y, transform.position.z);
        }
    }

    private void characterJump() {
        if (isGrounded && Input.GetButtonDown("Jump")) {
            mcsounds.playSound(MCSounds.JUMP_SOUND);
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, 25f);
        }
        animator.SetBool("Jump", !isGrounded);
    }

    private void isOnNextPlatform() {
        float marginError = 2f;
        if (this.transform.position.y > lastMCPosition && isGrounded) {
            lastMCPosition = this.transform.position.y + marginError;
            scorePanel.GetComponent<ScoreManager>().increaseScore();
            if (++levelReachedSound == 10) {
                mcsounds.playSound(5);
                levelReachedSound = 0;
            }
        }
    }

}
