﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MCSounds : MonoBehaviour {

    public static int JUMP_SOUND = 0;
    public static int DAMAGE_SOUND = 1; 
    public static int DEATH_SOUND = 2;
    public static int LIVES_SOUND = 3;
    public static int INVULNERABILITY_SOUND = 4; 
    public static int LEVEL_REACHED_SOUND = 5;

    private AudioSource asComponent;
    public AudioClip[] acs;
    
    void Start () {
        asComponent = GetComponent<AudioSource>();
    }

    public void playSound(int index) {
        if ( getConditionToPlaySound(index) ) {
            asComponent.clip = acs[index];
            asComponent.Play();
        }
    }

    private bool getConditionToPlaySound(int index)
    {
        if (index == JUMP_SOUND && isPlayingNotJumpSound()) return false;
        if (index == LEVEL_REACHED_SOUND && isPlayingNotJumpSound()) return false;
        return true;
    }

    private bool isPlayingNotJumpSound() { return (this.asComponent.isPlaying && asComponent.clip != acs[JUMP_SOUND]); }
}
