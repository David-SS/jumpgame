﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MCStatus : MonoBehaviour {
    
    public GameObject particleSystemObject;
    public GameObject brilliantEyes;
    private Animator animator;

    private float endOfGameDelay = 2f;
    private float timeBetweenDamage = 2f;
    private float invulnerabilityTime = 10f;

    public int lives;
    private bool canReceiveDamage = true;
    public GameObject livePanel;

    private MCSounds mcsounds;
    
    void Start() {
        lives = 3;
        animator = GetComponent<Animator>();
        mcsounds = GetComponent<MCSounds>();
    }

    IEnumerator timeToDamage() {
        yield return new WaitForSeconds(timeBetweenDamage);
        canReceiveDamage = true;
    }

    IEnumerator timeToLoseInvulnerability() {
        yield return new WaitForSeconds(invulnerabilityTime);
        canReceiveDamage = true;
        brilliantEyes.SetActive(false);
        particleSystemObject.GetComponent<ParticleSystem>().Stop();
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("ChangeableColorComponent")) {
            go.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 1);
        }
    }

    public void receiveDamage() {
        if (canReceiveDamage) {
            if (lives == 1) {
                mcsounds.playSound(MCSounds.DEATH_SOUND);
                animator.SetTrigger("Die");
                moveOnDead();
                Invoke("endOfGame", endOfGameDelay);
            }
            else {
                mcsounds.playSound(MCSounds.DAMAGE_SOUND);
                animator.SetTrigger("Hurt");
                moveOnDamage();
                canReceiveDamage = false;
            }
            lives--;
            livePanel.GetComponent<LivesManager>().setLives(lives);
            StopAllCoroutines();
            StartCoroutine(timeToDamage());
        }
    }
    void endOfGame() { SceneManager.LoadScene("EndOfGame"); }
    void moveOnDead() {
        Vector3 currentVelocity = GetComponent<Rigidbody2D>().velocity;
        GetComponent<Rigidbody2D>().velocity = new Vector3(0f, 20f, 0f);
        GetComponent<BoxCollider2D>().enabled = false;
    }
    void moveOnDamage() {
        Vector3 currentVelocity = GetComponent<Rigidbody2D>().velocity;
        GetComponent<Rigidbody2D>().velocity = new Vector3(0f, 20f, 0f);
    }
    

    public void getInvulnerability() {
        mcsounds.playSound(MCSounds.INVULNERABILITY_SOUND);
        canReceiveDamage = false;
        brilliantEyes.SetActive(true);
        particleSystemObject.GetComponent<ParticleSystem>().Play();
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("ChangeableColorComponent")) {
            go.GetComponent<Renderer>().material.color = new Color(1, 1, 0, 1);
        }
        StopAllCoroutines();
        StartCoroutine(timeToLoseInvulnerability());
    }

    public void increaseLives() {
        mcsounds.playSound(MCSounds.LIVES_SOUND);
        lives++;
        livePanel.GetComponent<LivesManager>().setLives(lives);
    }

    public bool isAlive() {
        if (lives <= 0) return false;
        else return true;
    }
}
